package pl.codementors.JPADragons.entityManagerFactory;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class DragonsEntityManagerFactory {
    private static final EntityManagerFactory emf = Persistence.createEntityManagerFactory("DragonsPU");

    /**
     *
     * @return entity manager.
     */
    public static final EntityManager createEntityManager(){
        return emf.createEntityManager();
    }

    /**
     * Close factory.
     */
    public static void close(){
        emf.close();
    }
}
