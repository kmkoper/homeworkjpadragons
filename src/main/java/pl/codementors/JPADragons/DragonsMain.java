package pl.codementors.JPADragons;

import pl.codementors.JPADragons.DAO.AdornmentsDao;
import pl.codementors.JPADragons.DAO.DragonsDao;
import pl.codementors.JPADragons.DAO.EggsDao;
import pl.codementors.JPADragons.DAO.LandsDao;
import pl.codementors.JPADragons.entityManagerFactory.DragonsEntityManagerFactory;
import pl.codementors.JPADragons.models.Adornment;
import pl.codementors.JPADragons.models.Dragon;
import pl.codementors.JPADragons.models.Egg;
import pl.codementors.JPADragons.models.Land;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class DragonsMain {
    private static final DragonsDao dao = new DragonsDao();
    private static final EggsDao eggDao = new EggsDao();
    private static final AdornmentsDao adornmentsDao = new AdornmentsDao();
    private static final LandsDao landsDao = new LandsDao();

    public static void main(String[] args) {

//        preparing();

        /**
         * Deleting dragons
         */
//        Dragon dragon = new Dragon();
//        for (int i = 25; i <= 36; i++) {
//            dragon.setDragonID(i);
//            dao.delete(dragon);
//        }
        /**
         * Deleting lands
         */
//        Land land = new Land();
//        for (int i = 1; i <= 12; i++) {
//            land.setLandID(i);
//            landsDao.delete(land);
//        }

        /**
         * Deleting eggs.
         */
//        Egg egg = new Egg();
//        egg.setEggID(15);
//        eggDao.delete(egg);


        dao.searchByName("Dygir").forEach(System.out::println);
        System.out.println("^^Searching by name^^");

        dao.searchByColor("Brown").forEach(System.out::println);
        System.out.println("^^Searching by color^^");

        eggDao.eggsHeavierThan(290).forEach(System.out::println);
        System.out.println("^^Searching eggs heavier than ..^^");

        dao.findAll().forEach(System.out::println);
        System.out.println("^^find all^^");

        landsDao.searchingDragonsByLandsName("oswia").forEach(System.out::println);
        System.out.println("^^Searching Dragons by they land name^^");
        DragonsEntityManagerFactory.close();


    }

    /**
     * Method to add first set of dragons.
     */

    private static void preparing() {
        Dragon dygir = new Dragon("Dygir", "Green", 200);
        Dragon karmel = new Dragon("Karmel", "Brown", 150);

        Egg egg = new Egg();
        egg.setDiameter(25);
        egg.setWeight(320);
        egg.setEggDragonID(dygir);
        Adornment adornment = new Adornment("Stripped", "Blue", egg);
        egg.setAdornment(adornment);


        Egg egg2 = new Egg();
        egg2.setWeight(300);
        egg2.setDiameter(20);
        egg2.setEggDragonID(dygir);
        dygir.setEggs(Arrays.asList(egg, egg2));
        Adornment adornment2 = new Adornment("Mesh", "pink", egg2);
        egg2.setAdornment(adornment2);

        Egg egg3 = new Egg();
        egg3.setWeight(230);
        egg3.setDiameter(30);
        egg3.setEggDragonID(karmel);
        karmel.setEggs(Arrays.asList(egg3));
        Adornment adornment3 = new Adornment("Dotted", "yellow", egg3);
        egg3.setAdornment(adornment3);

        Land oswia = new Land("oswia");
        Land froze = new Land("froze");
        Land oscyae = new Land("oscyae");
        dygir.setLands(Stream.of(oswia, froze).collect(Collectors.toSet()));
        karmel.setLands(Stream.of(oscyae).collect(Collectors.toSet()));


        dao.persist(dygir);
        dao.persist(karmel);
    }
}
