package pl.codementors.JPADragons.models;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;


@Entity
@Table(name = "dragons")
public class Dragon {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int dragonID;

    @Column
    private String name;

    @Column
    private String color;

    @Column
    private double wingSpan;

    public Dragon() {
    }

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "eggDragonID", fetch = FetchType.EAGER, orphanRemoval = true)
    private List<Egg> eggs = new ArrayList<>();

    /**
     * many to many relation
     * @JoinTable - joining tables
     * @JoinColumn - foreign key from side owning relation
     * @InverseJoinColumn - foreign key from second side of relation.
     * Hibernate automatically take primary key from tables and use them to make foreign keys.
     * @FethType.Eager - if something will be changed in parents that will automatically affect children.
     */
    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "dragonsLands", joinColumns=@JoinColumn(name="dlDragonID"), inverseJoinColumns = @JoinColumn(name = "dlLandID"))
    private Set<Land> lands;

    public Dragon(String name, String color, double wingSpan) {
        this.name = name;
        this.color = color;
        this.wingSpan = wingSpan;
    }

    public int getDragonID() {
        return dragonID;
    }

    public Dragon setDragonID(int dragonID) {
        this.dragonID = dragonID;
        return this;
    }

    public String getName() {
        return name;
    }

    public Dragon setName(String name) {
        this.name = name;
        return this;
    }

    public String getColor() {
        return color;
    }

    public Dragon setColor(String color) {
        this.color = color;
        return this;
    }

    public double getWingSpan() {
        return wingSpan;
    }

    public Dragon setWingSpan(double wingSpan) {
        this.wingSpan = wingSpan;
        return this;
    }
    public List<Egg> getEggs() {
        return eggs;
    }

    public Dragon setEggs(List<Egg> eggs) {
        this.eggs = eggs;
        return this;
    }

    public Set<Land> getLands() {
        return lands;
    }

    public Dragon setLands(Set<Land> lands) {
        this.lands = lands;
        return this;
    }

    @Override
    public String toString() {
        return "Dragon{" +
                "dragonID=" + dragonID +
                ", name='" + name + '\'' +
                ", color='" + color + '\'' +
                ", wingSpan=" + wingSpan +
                ", eggs=" + eggs +
                ", lands=" + lands +
                '}';
    }
}

