package pl.codementors.JPADragons.models;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "lands")
public class Land {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int landID;

    @Column
    private String landName;

    @ManyToMany(cascade = CascadeType.ALL, mappedBy = "lands", fetch = FetchType.EAGER)
    private Set<Dragon> dragons;

    public Land(String landName) {
        this.landName = landName;
    }

    public Land() {
    }

    public int getLandID() {
        return landID;
    }

    public Land setLandID(int landID) {
        this.landID = landID;
        return this;
    }

    public String getLandName() {
        return landName;
    }

    public Land setLandName(String landName) {
        this.landName = landName;
        return this;
    }

    public Set<Dragon> getDragons() {
        return dragons;
    }

    public Land setDragons(Set<Dragon> dragons) {
        this.dragons = dragons;
        return this;
    }

    @Override
    public String toString() {
        return "Land{" +
                "landID=" + landID +
                ", landName='" + landName + '\'' +
                '}';
    }
}
