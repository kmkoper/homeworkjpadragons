package pl.codementors.JPADragons.models;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "adornment")
public class Adornment implements Serializable {

    @Column
    private String pattern;

    @Column
    private String color;

    @Id
    @MapsId
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "adornmentID", referencedColumnName = "eggID")
    private Egg adornmentID;

    public String getPattern() {
        return pattern;
    }

    public Adornment setPattern(String pattern) {
        this.pattern = pattern;
        return this;
    }

    public String getColor() {
        return color;
    }

    public Adornment setColor(String color) {
        this.color = color;
        return this;
    }

    public Egg getAdornmentID() {
        return adornmentID;
    }

    public Adornment setAdornmentID(Egg adornmentID) {
        this.adornmentID = adornmentID;
        return this;
    }

    public Adornment(String pattern, String color, Egg adornmentID) {
        this.pattern = pattern;
        this.color = color;
        this.adornmentID = adornmentID;
    }

    public Adornment() {
    }

    @Override
    public String toString() {
        return "Adornment{" +
                "adornmentID=" + adornmentID.getEggID() +
                ", pattern='" + pattern + '\'' +
                ", color=" + color +
                '}';
    }
}
