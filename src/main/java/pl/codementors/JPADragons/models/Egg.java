package pl.codementors.JPADragons.models;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "eggs")
public class Egg {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int eggID;

    @Column
    private double diameter;

    @Column
    private double weight;


    /**
     * Many to one relation. Egg can have only one dragon. Dragon can have many eggs.
     */
    @ManyToOne
    @JoinColumn(name = "eggDragonID", referencedColumnName = "dragonID")
    private Dragon eggDragonID;

    /**
     * One to one relation. Egg can only have one adornment. Orphan Removal means that
     * if you delete egg or adornment other side of relation is removed automatically.
     */
    @OneToOne(mappedBy = "adornmentID", cascade = CascadeType.ALL, orphanRemoval = true)
    @PrimaryKeyJoinColumn
    private Adornment adornment;

    public Adornment getAdornment() {
        return adornment;
    }

    public Egg setAdornment(Adornment adornment) {
        this.adornment = adornment;
        return this;
    }

    public int getEggID() {
        return eggID;
    }

    public Egg setEggID(int eggID) {
        this.eggID = eggID;
        return this;
    }

    public double getDiameter() {
        return diameter;
    }

    public Egg setDiameter(double diameter) {
        this.diameter = diameter;
        return this;
    }

    public double getWeight() {
        return weight;
    }

    public Egg setWeight(double weight) {
        this.weight = weight;
        return this;
    }

    public Dragon getEggDragonID() {
        return eggDragonID;
    }

    public Egg setEggDragonID(Dragon eggDragonID) {
        this.eggDragonID = eggDragonID;
        return this;
    }

    public Egg() {
    }

    public Egg(double diameter, double weight, Dragon eggDragonID) {
        this.diameter = diameter;
        this.weight = weight;
        this.eggDragonID = eggDragonID;
    }

    @Override
    public String toString() {
        return "Egg{" +
                "eggID=" + eggID +
                ", diameter=" + diameter +
                ", weight=" + weight +
                ", eggDragonID=" + eggDragonID.getDragonID() +
                ", adornment=" + adornment +
                '}';
    }
}
