package pl.codementors.JPADragons.DAO;

import pl.codementors.JPADragons.entityManagerFactory.DragonsEntityManagerFactory;
import pl.codementors.JPADragons.models.Adornment;
import pl.codementors.JPADragons.models.Dragon;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

public class AdornmentsDao {
    /**
     * deleting adornments.
     * @param adornment
     */
    public void delete(Adornment adornment){
        EntityManager em = DragonsEntityManagerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.remove(em.merge(adornment));
        tx.commit();
        em.close();
    }
}
