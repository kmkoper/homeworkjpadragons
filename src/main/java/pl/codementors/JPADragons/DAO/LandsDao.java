package pl.codementors.JPADragons.DAO;

import pl.codementors.JPADragons.entityManagerFactory.DragonsEntityManagerFactory;
import pl.codementors.JPADragons.models.Dragon;
import pl.codementors.JPADragons.models.Dragon_;
import pl.codementors.JPADragons.models.Land;
import pl.codementors.JPADragons.models.Land_;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Optional;

public class LandsDao {
    public void delete(Land land) {
        EntityManager em = DragonsEntityManagerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.remove(em.merge(land));
        tx.commit();
        em.close();
    }

    /**
     * Searching dragons by lands name
     * @param landName land name
     * @return list of finded dragons.
     */

    public List<Dragon> searchingDragonsByLandsName(String landName) {
        EntityManager em = DragonsEntityManagerFactory.createEntityManager();
        Query query = em.createQuery("SELECT d FROM Dragon d JOIN d.lands l WHERE l.landName = :landName");
        query.setParameter("landName", landName);
        List<Dragon> dragonList = query.getResultList();
        em.close();
        return dragonList;
    }
}