package pl.codementors.JPADragons.DAO;


import pl.codementors.JPADragons.entityManagerFactory.DragonsEntityManagerFactory;
import pl.codementors.JPADragons.models.Dragon;
import pl.codementors.JPADragons.models.Dragon_;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Optional;


public class DragonsDao {
    /**
     *
     * @param dragon Dragon to be saved in database
     * @return Dragon
     */
    public Dragon persist(Dragon dragon){
        EntityManager em = DragonsEntityManagerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.persist(dragon);
        tx.commit();
        em.close();
        return dragon;
    }

    /**
     *
     * @param dragon Dragon to be updated in database
     * @return updated Dragon
     */
    public Dragon merge(Dragon dragon){
        EntityManager em = DragonsEntityManagerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.merge(dragon);
        tx.commit();
        em.close();
        return dragon;
    }

    /**
     * Deleting Dragon from database.
     * @param dragon
     */
    public void delete(Dragon dragon){
        EntityManager em = DragonsEntityManagerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.remove(em.merge(dragon));
        tx.commit();
        em.close();
    }

    /**
     * Search Dragons by name CRITERIA API.
     * @return finded Dragon
     */
    public List<Dragon> searchByName(String name){
        EntityManager em = DragonsEntityManagerFactory.createEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Dragon> query = cb.createQuery(Dragon.class);
        Root<Dragon> root = query.from(Dragon.class);
        query.where(root.get(Dragon_.name).in(name));
        List<Dragon> dl = em.createQuery(query).getResultList();
        em.close();
        return dl;
    }

    /**
     * Search Dragons by they color CRITERIA API.
     * @param color which coloured dragons are you searching?
     * @return List of dragons in searched color.
     */
    public List<Dragon> searchByColor(String color){
        EntityManager em = DragonsEntityManagerFactory.createEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Dragon> query = cb.createQuery(Dragon.class);
        Root<Dragon> root = query.from(Dragon.class);
        query.where(root.get(Dragon_.color).in(color));
        List<Dragon> dl = em.createQuery(query).getResultList();
        em.close();
        return dl;
    }

    /**
     *
     * @return List of all dragons JPQL.
     */

    public List<Dragon> findAll(){
        EntityManager em = DragonsEntityManagerFactory.createEntityManager();
        Query query = em.createQuery("SELECT d FROM Dragon d");
        List<Dragon> dl = query.getResultList();
        em.close();
        return dl;
    }

    /**
     * Finding dragon by its ID.
     * @param dragonID dragon ID
     * @return Finded dragon.
     */
    public Dragon find(int dragonID){
        EntityManager em = DragonsEntityManagerFactory.createEntityManager();
        Dragon dragon = em.find(Dragon.class, dragonID);
        em.close();
        return dragon;
    }
}
