package pl.codementors.JPADragons.DAO;

import pl.codementors.JPADragons.entityManagerFactory.DragonsEntityManagerFactory;
import pl.codementors.JPADragons.models.Egg;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.util.List;

public class EggsDao {
    /**
     * Deleting eggs
     * @param egg egg to delete
     */

    public void delete(Egg egg){
        EntityManager em = DragonsEntityManagerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.remove(em.merge(egg));
        tx.commit();
        em.close();
    }

    /**
     * Finding eggs heavier than using JPQL
     * @param weight
     * @return list of eggs heavier than param.
     */
    public List<Egg> eggsHeavierThan(double weight){
        EntityManager em = DragonsEntityManagerFactory.createEntityManager();
        Query query = em.createQuery("SELECT e FROM Egg e WHERE e.weight >:weight");
        query.setParameter("weight", weight);
        List<Egg> eggsList = query.getResultList();
        em.close();
        return eggsList;
    }
}
