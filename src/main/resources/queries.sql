--CREATE USER 'homework1' IDENTIFIED BY '1234';
--CREATE DATABASE homeworkDB CHARACTER SET utf8 COLLATE utf8_general_ci;
--GRANT ALL ON homeworkDB.* to 'homework1';
--USE homeworkDB;

CREATE TABLE dragons (
    name VARCHAR(100),
    color VARCHAR(100),
    wingSpan NUMERIC(3,0),
    dragonID INT(11) PRIMARY KEY AUTO_INCREMENT
);

CREATE TABLE eggs (
    weight NUMERIC(3,0),
    diameter  NUMERIC(3,0),
    eggID INT(11) PRIMARY KEY AUTO_INCREMENT,
    eggDragonID INT(11),
    FOREIGN KEY (eggDragonID) REFERENCES dragons (dragonID)
);

CREATE TABLE adornment (
    color VARCHAR(100),
    pattern VARCHAR(100),
    adornmentID INT(11),
    PRIMARY KEY (adornmentID),
    FOREIGN KEY (adornmentID) REFERENCES eggs (eggID)
);

CREATE TABLE lands (
    landName VARCHAR(100),
    landID INT(11) PRIMARY KEY AUTO_INCREMENT
);

CREATE TABLE dragonsLands (
    dlDragonID INT(11),
    dlLandID INT(11),
    PRIMARY KEY (dlDragonID, dlLandID),
    FOREIGN KEY (dlDragonID) REFERENCES dragons (dragonID),
    FOREIGN KEY (dlLandID) REFERENCES lands (landID)
);
